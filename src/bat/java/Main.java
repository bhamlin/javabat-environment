package bat.java;

import org.reflections.Reflections;
import bat.java.arrays.Unlucky;
import bat.java.model.RunTest;

public class Main {

    public static void main(String... argv) {
        var refl = new Reflections("bat.java.arrays");
        var list = refl.getTypesAnnotatedWith(RunTest.class);
        for (var clazz : list) {
            System.out.println(clazz.toString());
        }

        final var obj = new Unlucky();

        obj.run();
    }

}