package bat.java.model;

public class TestCase<Input, Output> {

    final public Input input;
    final public Output output;

    public TestCase(Input input, Output output) {
        this.input = input;
        this.output = output;
    }

}
