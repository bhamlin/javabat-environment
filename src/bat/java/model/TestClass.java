package bat.java.model;

import java.util.function.Function;

public abstract class TestClass<Input, Output> implements Runnable {

    protected abstract Output problem(Input input);

    protected void execute_tests(String label, Iterable<TestCase<Input, Output>> test_cases, Function<Input, String> fn) {
        for (var test_case : test_cases) {
            final Input input = test_case.input;
            final Output expected = test_case.output;
            final Output actual = this.problem(input);

            final var arg_line = String.format("%s(%s)", label, fn.apply(input));
            final var status = (expected.equals(actual)) ? ("✓") : ("x");

            System.out.printf("%30s -> [%s] %s (%s)", arg_line, status, actual, expected);
            System.out.println();
        }
    }

}
