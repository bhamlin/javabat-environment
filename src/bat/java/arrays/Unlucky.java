package bat.java.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import bat.java.model.TestCase;
import bat.java.model.TestClass;

public class Unlucky extends TestClass<int[], Boolean> {

    @Override
    public Boolean problem(int[] nums) {
        final int len = nums.length;
        final ArrayList<Boolean> results = new ArrayList<>();

        // Check to see if position 0 is possible and unlucky
        if (len > 1) {
            results.add(check_at(nums, 0));
        }
        // Check to see if position 1 is possible and unlucky
        if (len > 2) {
            results.add(check_at(nums, 1));
        }
        // // Check to see if next to last position is worth checking and unlucky
        // if (len > 4) {
        // results.add(check_at(nums, len - 3));
        // }
        // Check to see if last position is worth checking and unlucky
        if (len > 3) {
            results.add(check_at(nums, len - 2));
        }

        // System.out.printf("%s, %s", Arrays.toString(nums), results.toString());
        // System.out.println();

        for (boolean result : results) {
            if (result) {
                return true;
            }
        }
        return false;
    }

    boolean check_at(int[] input, int index) {
        return (input[index] == 1) && (input[index + 1] == 3);
    }

    @Override
    public void run() {
        final ArrayList<TestCase<int[], Boolean>> test_cases = new ArrayList<>();

        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 3, 4, 5 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 2, 1, 3, 4, 5 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 1, 1 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 3, 1 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 1, 3 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 2, 3 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 3, 3, 3 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 3 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 4 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] {}, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 1, 1, 3, 1 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 1, 3, 1, 1 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 1, 1, 1, 3 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 4, 1, 5 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 1, 2, 3 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 2, 3, 2, 1 }, false));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 2, 3, 1, 3 }, true));
        test_cases.add(new TestCase<int[], Boolean>(new int[] { 1, 2, 3, 4, 1, 3 }, true));

        this.execute_tests("unlucky1", test_cases, n -> (Arrays.toString(n)));
    }

}